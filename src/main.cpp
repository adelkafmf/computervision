#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>

uchar getBinaryPattern(cv::Mat img, int i, int j);

int main(int argc, char** argv)
{
    if(argc < 1) return 1;
    std::string path_img = argv[1];
    cv::Mat img;
    cv::Mat lbp;
    img = cv::imread(path_img, 0);
    lbp = img.clone();
    for(int i = 1;i < img.rows - 1;i++)
        for(int j = 1;j < img.cols - 1;j++)
            lbp.at<uchar>(i, j) = getBinaryPattern(img, i, j);
    cv::namedWindow("Gray");
    cv::namedWindow("LBP");
    cv::imshow("Gray", img);
    cv::imshow("LBP", lbp);
    cv::waitKey();
    cv::destroyAllWindows();
    return 0;
}

uchar getBinaryPattern(cv::Mat img, int i, int j)
{
    uchar pattern = 0;
    uchar order = 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i-1,j))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i-1,j+1))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i,j+1))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i+1,j+1))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i+1,j))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i+1,j-1))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i,j-1))
        pattern += order;
    order = order << 1;
    if(img.at<uchar>(i,j) <= img.at<uchar>(i-1,j-1))
        pattern += order;
    return pattern;
}